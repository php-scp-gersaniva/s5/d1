<?php 

$tasks = ['Get git', 'Bake HTML', 'Eat CSS', 'Learn PHP'];


// isset checks if a given super global variable is present
//a super global variable in PHP is a special kind of variable that can persist data from client to server (or vice-versa)

if(isset($_GET['index'])){
    // echo $_GET['index'];

    $indexGet = $_GET['index'];
    echo "The retrieved task from GET is $tasks[$indexGet]";
}

if(isset($_POST['index'])){
    //echo $_POST['index'];

    $indexPost = $_POST['index'];
    echo "The retrieved task from POST is $tasks[$indexPost]";
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP SC S5</title>
</head>
<body>
        <h1>Task index from GET</h1>

        <?php //HTML form method GET allows data in forms to be sent via URL parameters 
        ?>
        <form method="GET">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <h1>Task index from POST</h1>

        <form method="POST">

        <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
        </select>

            <button type="submit">POST</button>

        </form>
</body>
</html>